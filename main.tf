terraform {
  cloud {
    organization = "kyawzin-workspace-cohort7"

    workspaces {
      name = "cohort7-workspace"
    }
  }
}

resource "docker_network" "private_network" {
  name = "my_network"
  ipam_config {
    subnet  = "172.18.0.0/24"
    gateway = "172.18.0.254"
  }
}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

########################ClientTestMachine###########################

resource "docker_container" "ClientTestMachine" {
  image = docker_image.nginx.image_id
  name  = "testmachine"
  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.16"
  }

}

###################ForWebServers################################

resource "docker_container" "webserver1" {
  image = docker_image.nginx.image_id
  name  = "webserver1"
  ports {
    internal = 80
    external = 8001
  }
  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.13"
  }
  provisioner "local-exec" {
    command = "docker exec ${self.name} sh -c 'echo \"This is Web Server-1\" > /usr/share/nginx/html/index.html'"
  }
}

resource "docker_container" "webserver2" {
  image = docker_image.nginx.image_id
  name  = "webserver2"
  ports {
    internal = 80
    external = 8002
  }
  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.14"
  }
  provisioner "local-exec" {
    command = "docker exec ${self.name} sh -c 'echo \"This is Web Server-2\" > /usr/share/nginx/html/index.html'"
  }
}

resource "docker_container" "webserver3" {
  image = docker_image.nginx.image_id
  name  = "webserver3"
  ports {
    internal = 80
    external = 8003
  }
  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.15"
  }
  provisioner "local-exec" {
    command = "docker exec ${self.name} sh -c 'echo \"This is Web Server-3\" > /usr/share/nginx/html/index.html'"
  }
}

########################ForKeepalivedLoadBalancer###########################

resource "docker_image" "centos_keepalived" {
  name = "centos-keepalived:latest"
  build {
    context = "${path.module}/" # Path to the directory containing the Dockerfile
  }
}

resource "docker_container" "LB1" {
  name       = "LB1"
  image      = docker_image.centos_keepalived.image_id
  privileged = true

  volumes {
    host_path      = "/home/vagrant/ha-lb-proj/keepalived-LB1/"
    container_path = "/etc/keepalived"
    read_only      = true
  }
  volumes {
    host_path      = "/home/vagrant/ha-lb-proj/keepalived-LB1/"
    container_path = "/etc/nginx"
    read_only      = true
  }

  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.11"
  }
  // Ensure keepalived starts when the container starts
  command = [
    "sh", "-c",
    "keepalived && /usr/sbin/init"
  ]
}
resource "docker_container" "LB2" {
  depends_on = [docker_container.LB1]
  name       = "LB2"
  image      = docker_image.centos_keepalived.image_id
  privileged = true

  volumes {
    host_path      = "/home/vagrant/ha-lb-proj/keepalived-LB2/"
    container_path = "/etc/keepalived"
    read_only      = true
  }
  volumes {
    host_path      = "/home/vagrant/ha-lb-proj/keepalived-LB2/"
    container_path = "/etc/nginx"
    read_only      = true
  }

  networks_advanced {
    name         = docker_network.private_network.name
    ipv4_address = "172.18.0.12"
  }
  // Ensure keepalived starts when the container starts
  command = [
    "sh", "-c",
    "keepalived && /usr/sbin/init"
  ]
}