FROM centos:latest

# Update repository URLs and install necessary packages
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-* && \
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-* && \
    yum install -y nginx keepalived vim net-tools initscripts && \
    yum clean all

# Copy scripts and set executable permissions
COPY check_nginx.sh /etc/keepalived/check_nginx.sh
COPY start_services.sh /opt/start_services.sh
RUN chmod +x /etc/keepalived/check_nginx.sh /opt/start_services.sh

# Enable services
RUN systemctl enable keepalived nginx

# Start services
CMD ["/opt/start_services.sh"]
