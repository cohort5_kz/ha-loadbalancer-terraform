#!/bin/bash start_services.sh

# Enable and start Nginx and Keepalived services
systemctl start nginx
systemctl start keepalived

# Keep the container running by tailing a log file
# tail -f /var/log/keepalived.log
